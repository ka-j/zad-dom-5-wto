import argparse

SZYFRY=['GA-DE-RY-PO-LU-KI',
        'PO-LI-TY-KA-RE-NU',
        'KA-CE-MI-NU-TO-WY',
        'KO-NI-EC-MA-TU-RY',
        'MO-TY-LE-CU-DA-KI',
        'BI-TW-AO-CH-MU-RY',
        'KU-LA-RY-MI-NE-TO',
        'KU-LO-PE-RY-ZA-GI',
        'NA-SZ-HU-FI-EC',
        'MA-LI-NO-WE-BU-TY']

deskrypcja = 'program który szyfruje/odszyfrowuje teksty szyframi podstawieniowymi.\nWybierasz jeden z domyślnych szyfrów wpisując jako drugi argument jego indeks czyli liczbę od 0 do 9. Zamiast tego możesz podać własną sekwencję, która będzie kodem. Musi składać się z dwuliterowych bloków, oddzielanych myślnikami. Każda litera może wystąpić w sekwencji tylko raz.'
temp = ""
for i, s in enumerate(SZYFRY):
    temp += f"\n\t{i}:{s},"

parser = argparse.ArgumentParser(description=deskrypcja, epilog=f'\nDostępne szyfry:{temp[:-1]}')
parser.add_argument('tekst', metavar='t', type=str, 
                    help='tekst który będzie szyfrowany/odszyfrowany')
parser.add_argument('szyfr_id', metavar='s', type=str, 
                    help='indeks szyfru, który ma być użyty, opcjonalnie sekwencja do użycia przy szyfrowaniu')

def szyfruj(tekst: str, szyfr: str):
    bloki = szyfr.upper().split(sep='-')
    kod = {}

    try:
        if len(bloki) == 1:
            raise Exception('nie ma dwuliterowych bloków oddzielonych myślnikami.')
        for blok in bloki:
            if len(blok) != 2:
                raise Exception('zawiera blok o długości innej niż 2.')
            if blok[0] in kod.keys() or blok[1] in kod.keys():
                raise Exception('pewna litera się powtarza.')
            kod[blok[0]] = blok[1]
            kod[blok[1]] = blok[0]
    except Exception as e:
        return f"To nie jest poprawna sekwencja do szyfru podmienianego, bo {e.args[0]}"
        
    else:
        zaszyfr = ''
        for l in tekst.upper():
            if l in kod.keys():
                zaszyfr += kod[l]
            else:
                zaszyfr += l
        return zaszyfr

args = vars(parser.parse_args())

tekst = args['tekst']
szyfr = args['szyfr_id']

try:
    szyfr = SZYFRY[int(szyfr)]
except ValueError:
    pass

print(f"{szyfruj(tekst, szyfr)}")
